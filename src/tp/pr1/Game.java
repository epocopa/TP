/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr1;

import java.util.Random;

public class Game {
	private Board board;
	private int size, initCells, score, highest, freeCounter;
	private Random random;
	private Position[] free;

	public Game(int size, int initCells, long seed) {
		this.size = size;
		this.initCells = initCells;
		this.random = new Random(seed);
		this.board = new Board(size);
		for (int i = 0; i < initCells; i++) {
			int v = newValue();
			if (v > highest){
				highest = v;
			}
		}
		
		this.freeCounter = size*size - initCells;
	}

	public int getHighest() {
		return highest;
	}

	public void move(Direction dir){

		MoveResults m = board.executeMove(dir);
		if (m.isMoved()){
			score += m.getPoints();
			if (highest < m.getMaxValue()){
				highest = m.getMaxValue();
			}
			newValue();
		}

	}

	public void reset(){
		board= new Board(size);
		score = 0;
		highest = 0;
		for (int i = 0; i < initCells; i++) {
			int v = newValue();
			if (v > highest){
				highest = v;
			}
		}
	}

	private int generateValue(){
		int value = 2;
		if( Math.random() > 0.9){
			value = 4;
		}
		return value;
	}


	private int newValue(){
		saveFree();
		int pos = (int) (Math.random() * freeCounter);
		Position p = free[pos];
		int v = generateValue();
		board.setCell(p, v);
		return v;
	}

	private void saveFree(){
		free = new Position[size*size];
		freeCounter = board.emptyCells(free, freeCounter);
	}

	public boolean canMove() {
		boolean canMove = true;
		
		if (freeCounter == 1 && !board.canMerge()) {
				canMove = false;
		}
		
		return canMove;
	}

	@Override
	public String toString() {
		return board.toString() +"\nScore: " + score + " Highest: " + highest;
	}
}
