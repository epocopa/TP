/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr1;

import java.util.Scanner;

public class Controller {
	private Game game;
	private Scanner in;

	public Controller(Game game, Scanner in) {
		this.game = game;
		this.in = in;
	}

	public void run(){
		boolean exit = false, won = false, lost = false, repeat;
		do {
			System.out.println(game.toString());
			do {
				System.out.print("~$ ");
				repeat = false;
				switch (in.next().toLowerCase()) {
					case "move":
						//To avoid unnecessary switch
						try {
							game.move(Direction.valueOf(in.next().toUpperCase()));
						} catch (IllegalArgumentException e) {
							//~$
							System.err.println("No such direction");
							repeat = true;
						}
						break;
					case "reset":
						game.reset();
						break;
					case "exit":
						exit = true;
						break;
					case "help":
						printHelp();
						repeat = true;
						break;
					default:
						System.out.println("Unknown command");
						//Prevent double "Unknown command", example input "mov up"
						in.nextLine();
						repeat = true;
				}
			} while (repeat);
			won =  game.getHighest() == 2048;
			lost = !game.canMove();
		} while (!exit  && !won && !lost);
		System.out.println(game.toString());
		if (won){
			System.out.println("Congratulations, you won :)");
		} else if(lost) {
			System.out.println("Nice try, you lost :(");
		}
	}
	public void printHelp(){
		System.out.println("Game 2048 Help: ");
		System.out.println("\tMove (direction) -> Executes a move in the desired direction");
		System.out.println("\tReset -> Restarts the game");
		System.out.println("\tHelp -> Display help information about the game");
		System.out.println("\tExit -> Exits the game");
	}
}
