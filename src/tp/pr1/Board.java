/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr1;

import tp.pr1.util.MyStringUtils;

public class Board {
	private Cell[][] board;
	private int boardSize;
	
	public Board(int boardSize) {
		this.boardSize = boardSize;
		board = new Cell[boardSize][boardSize];
		for(int i = 0; i < boardSize; i++){
			for(int j = 0; j < boardSize; j++){
				board[i][j] = new Cell(0);
			}
		}
	}
	
	public void setCell(Position pos, int value){
		board[pos.getRow()][pos.getColumn()].setValue(value);
	}

	public int getBoardSize() {
		return boardSize;
	}
	
	public MoveResults executeMove(Direction dir){
		MoveResults results;
		
		switch(dir){
			case DOWN:
				trans();
				mirror();
				results = move_left();
				mirror();
				trans();
				break;
			case LEFT:
				results = move_left();
				break;
			case RIGHT:
				mirror();
				results = move_left();
				mirror();
				break;
			case UP:
				trans();
				results = move_left();
				trans();
				break;
			default:
				results = move_left();
				break;
		}
		
		
		return results;
	}
	
	private MoveResults move_left() {
		boolean moved = false;
		int points = 0, maxValue = 0;
		
		// Push && Merge
		for(int i = 0; i < boardSize; i++) {
			int elm = 0;
			boolean lastMerged = true;
			if (!board[i][0].isEmpty()) {
				elm = 1;
				lastMerged = false;
			}
			for (int j = 1; j < boardSize; j++) {
				if (!board[i][j].isEmpty()) {
					if (!lastMerged && board[i][j].doMerge(board[i][elm-1])){
						points += board[i][elm-1].getValue();
						if (maxValue < board[i][elm-1].getValue()) {
							maxValue = board[i][elm-1].getValue();
						}
						lastMerged = true;
						moved = true;
					} else {
						if (elm != j) {
							board[i][elm].setValue(board[i][j].getValue());
							board[i][j].setValue(0);
							lastMerged = false;
							moved = true;
						}
						elm++;
					}
				}
			}
		}
		
		return new MoveResults(moved, points, maxValue);
	}
	
	private void mirror() {
		Cell aux;
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize/2; j++) {
				aux = board[i][j];
				board[i][j] = board[i][boardSize-1-j];
				board[i][boardSize-1-j] = aux;
			}
		}
	}
	
	private void trans() {
		Cell aux;
		for (int i = 0; i < boardSize; i++) {
			for (int j = i; j < boardSize; j++) {
				aux = board[i][j];
				board[i][j] = board[j][i];
				board[j][i] = aux;
			}
		}
	}

	public int emptyCells(Position[] free, int freeCounter){
		freeCounter = 0;
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if (board[i][j].isEmpty()){
					free[freeCounter] = new Position(i,j);
					freeCounter++;
				}
			}
		}
		return freeCounter;
	}
	
	public boolean canMerge() {
		boolean canMerge = false;
		
		for (int i = 0; i < boardSize - 1; i++) {
			for (int j = 0; j < boardSize - 1; j++) {
				if (board[i][j].canMerge(board[i][j+1])) {
					canMerge = true;
				} else if (board[i][j].canMerge(board[i+1][j])) {
					canMerge = true;
				}
			}
		}
		for (int k = 0; k < boardSize - 1; k++) {
			if (board[k][boardSize - 1].canMerge(board[k+1][boardSize - 1])) {
				canMerge = true;
			} else if (board[boardSize - 1][k].canMerge(board[boardSize - 1][k+1])) {
				canMerge = true;
			}
		}
		
		return canMerge;
	}

	@Override
	public String toString() {
		String result = "";
		int cellSize = 7;
		String space = " ";
		String vDelimiter = "|";
		String hDelimiter = "-";
	

		for (int i = 0; i < boardSize; i++) {
			result += "\n" + MyStringUtils.repeat(hDelimiter, boardSize*cellSize+boardSize+1) + "\n";
			for (int j = 0; j < boardSize; j++) {
				result += vDelimiter;
				result += MyStringUtils.centre(board[i][j].toString(), cellSize);
			}
			result += vDelimiter;
		}
		result += "\n" + MyStringUtils.repeat(hDelimiter, boardSize*cellSize+boardSize+1) + "\n";
		return result;
	}	
	
	

}
